Coding Test (fictitious)

Attached you’ll find the code for a simple mobile app to browse cakes. Unfortunately, the developer who wrote this code is no longer employed by the company as we had some concerns over his ability. The project compiles but the app crashes as soon as it runs.

The app loads a JSON feed containing a repeated list of cakes, with title, image and description from a URL, then displays the contents of the feed in a scrollable list.

We would like you to fix the crash bug, ensure the functionality of the app works as expected (all images display correctly on the table, all text is readable) and performs smoothly (ideally, this should be done without third party libraries).

Additionally, feel free to refactor and optimise the code where appropriate. Ideally, what we’d be looking for is:

* Simple fixes for crash bug(s)
* Application to support rotation
* Safe and efficient loading of images
* Removal of any redundant code
* Refactoring of code to best practices

This is your opportunity to show us how you think and Android app should be architected, please make any changes you feel would benefit the app.

The test should take around 2 hours, certainly no longer than 3 hours. Good luck!


Jean Power Updates

- General layout updates - item_list_layout had match_parent for text section, issue with RelativeLayout alignment identifiers, layout_width to dp, not px, text sizes to sp.
- Updated ListView to RecyclerView - more efficient, more powerful and customisable, better extensibility and enforces better programming style (with ViewHolder)
- RecyclerView can have asynchronous image loading issues, so tagged view passed in to ensure view was not recycled before the image was loaded.
- Asynctask for loading JSON and images, as long running processes should not be done on the main thread.
- Internet permission was missing, so was added, along with access_network_state, to give feedback to the user if they have no connection.
- JSON load and images load separately - this allows for better user experience, as they can see some progress on the screen.
- Fragment retains state for configuration changes
- StreamUtils no longer reads byte by byte, uses more efficient reading of chunks of bytes.
- ImageCache for caching Bitmaps.
- Due to required fixed size for ImageView, scaled the bitmaps so less memory used. Bitmap size is queried first, without allocation of memory, to allow for decoding at more appropriate size - this can help protect the app from out of memory errors from unknown size bitmaps. Used a fixed constant, as the image view was a fixed size. If not, would’ve made this more flexible.

Further updates required
- Would write full unit tests!
- If any flexibility in image size would be required, would make BitmapManipulator respond more dynamically to requested image sizes.
- Small amount of repeated code between JSONLoader and ImageLoader - would abstract this. Did not do for this app, as it would’ve required more classes to decode the bitmap off the UI thread (as it is a long running process), which I felt was not necessary for a small app.
- As Asynctask is spun off multiple times, and images are not cached until they’re loaded, we are downloading pictures in multiple on first load. Solution would be to cache the image URL, and before loading the image, check if the image URL was already being loaded.

