package com.waracle.androidtest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapManipulator {

    private static final int REQUIRED_SIZE_IN_PIXELS = 128;

    public Bitmap convertToScaledBitmap(byte[] data) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        calculateBitmapSize(data, options);
        calculateSampleSize(options);
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    private void calculateBitmapSize(byte[] data, BitmapFactory.Options options) {
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    private void calculateSampleSize(BitmapFactory.Options options) {
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        int sampleSize = 1;

        if (imageHeight > REQUIRED_SIZE_IN_PIXELS || imageWidth > REQUIRED_SIZE_IN_PIXELS) {
            imageHeight /= 2;
            imageWidth /= 2;
        }

        while ((imageHeight / sampleSize) > REQUIRED_SIZE_IN_PIXELS && (imageWidth / sampleSize) > REQUIRED_SIZE_IN_PIXELS) {
            imageHeight /= 2;
            imageWidth /= 2;
            sampleSize *= 2;
        }

        options.inSampleSize = sampleSize;
    }

}


