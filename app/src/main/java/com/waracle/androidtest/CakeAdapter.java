package com.waracle.androidtest;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;

public class CakeAdapter extends RecyclerView.Adapter<CakeAdapter.ViewHolder> implements ErrorListener {

    private static final String TAG = CakeAdapter.class.getSimpleName();

    private Cake[] cakes;
    private ImageCache imageCache;
    private ErrorListener listener;

    public CakeAdapter(ErrorListener listener) {
        this.imageCache = new ImageCache();
        this.listener = listener;
    }

    public void setCakes(Cake[] cakes) {
        this.cakes = cakes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        resetImage(holder);

        try {
            Cake cake = cakes[position];

            holder.title.setText(cake.getTitle());
            holder.description.setText(cake.getDescription());

            holder.imageContainer.setTag(cake.getImageUrl());
            new ImageLoader(imageCache, holder.imageContainer, this).execute(new URL(cake.getImageUrl()));

        } catch (MalformedURLException e) {
            onError();
            Log.e(TAG, e.getMessage());
        }
    }

    private void resetImage(ViewHolder holder) {
        holder.progressBar.setVisibility(View.VISIBLE);
        holder.image.setImageBitmap(null);
    }

    @Override
    public int getItemCount() {
        return cakes.length;
    }

    @Override
    public void onError() {
        listener.onError();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView description;
        public LinearLayout imageContainer;
        public ProgressBar progressBar;
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            title = TextView.class.cast(itemView.findViewById(R.id.title));
            description = TextView.class.cast(itemView.findViewById(R.id.desc));
            imageContainer = LinearLayout.class.cast(itemView.findViewById(R.id.image_container));
            progressBar = ProgressBar.class.cast(itemView.findViewById(R.id.image_progress));
            image = ImageView.class.cast(itemView.findViewById(R.id.image_view));

        }
    }
}
