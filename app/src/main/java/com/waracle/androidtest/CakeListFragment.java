package com.waracle.androidtest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

public class CakeListFragment extends Fragment implements JSONLoaderListener, ErrorListener {

    private static final String TAG = CakeListFragment.class.getSimpleName();

    private static final String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";
    private static final String JSON_TITLE = "title";
    private static final String JSON_DESC = "desc";
    private static final String JSON_IMAGE_URL = "image";

    private CakeAdapter cakeAdapter;
    private RecyclerView recyclerView;

    public CakeListFragment() { /**/ }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        cakeAdapter = new CakeAdapter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = RecyclerView.class.cast(rootView.findViewById(R.id.list));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    @Override
    public void onLoadFinished(JSONArray array) {
        cakeAdapter.setCakes(getCakeArray(array));
        recyclerView.setAdapter(cakeAdapter);
    }

    public void handleRefresh() {
        loadData();
    }

    private void loadData() {
        JSONLoader loader = new JSONLoader(this);

        try {
            loader.execute(new URL(JSON_URL));
        } catch (MalformedURLException e) {
            onError();
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onError() {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.error_message)
                .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadData();
                    }
                })
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                }).show();

    }

    private Cake[] getCakeArray(JSONArray array) {
        Cake[] cakes = new Cake[array.length()];

        for (int i = 0; i < array.length(); i++) {
            Cake cake = new Cake();

            try {
                JSONObject object = JSONObject.class.cast(array.get(i));

                cake.setDescription(object.getString(JSON_DESC));
                cake.setTitle(object.getString(JSON_TITLE));
                cake.setImageUrl(object.getString(JSON_IMAGE_URL));

            } catch (JSONException e) {
                onError();
                Log.e(TAG, e.getMessage());
            }

            cakes[i] = cake;

        }

        return cakes;
    }

}