package com.waracle.androidtest;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class ImageCache {

    private static final int KILOBYTES = 1028;
    private static final int FRACTION_OF_MEMORY_USED = 8;
    private LruCache<String, Bitmap> cachedImages;

    public ImageCache() {
        setUpCache();
    }

    public void addImageToCache(String key, Bitmap bitmap) {
        if (getImageFromKey(key) == null) {
            cachedImages.put(key, bitmap);
        }
    }

    public Bitmap getImageFromKey(String key) {
        return cachedImages.get(key);
    }

    private void setUpCache() {
        cachedImages = new LruCache<String, Bitmap>(getMaxCacheSize()) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return ((bitmap.getRowBytes() * bitmap.getHeight()) / KILOBYTES);
            }
        };
    }

    private int getMaxCacheSize() {
        int maxMemory = (int) Runtime.getRuntime().maxMemory() / KILOBYTES;
        return maxMemory / FRACTION_OF_MEMORY_USED;
    }
}
