package com.waracle.androidtest;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageLoader extends AsyncTask<URL, Void, Bitmap> {

    private static final String TAG = ImageLoader.class.getSimpleName();

    private final WeakReference<LinearLayout> container;
    private ErrorListener listener;
    private URL url;
    private ImageCache imageCache;

    public ImageLoader(ImageCache imageCache, LinearLayout container, ErrorListener listener) {
        this.listener = listener;
        this.container = new WeakReference<>(container);
        this.imageCache = imageCache;
    }

    @Override
    protected Bitmap doInBackground(URL... params) {

        url = params[0];
        Bitmap bitmap;

        if (isImageCached()) {
            bitmap = loadImageFromCache();
        } else {
            bitmap = loadImageFromUrl();
            cacheImage(bitmap);
        }

        return bitmap;
    }

    @Override
    public void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {

            if (!isImageViewRecycled()) {
                ImageView image = ImageView.class.cast(container.get().findViewById(R.id.image_view));
                image.setImageBitmap(bitmap);
                image.setVisibility(View.VISIBLE);
                container.get().findViewById(R.id.image_progress).setVisibility(View.GONE);
            }
        } else {
            listener.onError();
        }
    }

    private Bitmap loadImageFromUrl() {
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        byte[] array = new byte[]{};

        try {
            urlConnection = HttpURLConnection.class.cast(url.openConnection());
            inputStream = new BufferedInputStream(urlConnection.getInputStream());
            array = StreamUtils.readUnknownFully(inputStream);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            StreamUtils.close(inputStream);
            StreamUtils.close(urlConnection);
        }

        return new BitmapManipulator().convertToScaledBitmap(array);
    }

    private Bitmap loadImageFromCache() {
        return imageCache.getImageFromKey(url.toString());
    }

    private boolean isImageCached() {
        return imageCache.getImageFromKey(url.toString()) != null;
    }

    private void cacheImage(Bitmap bitmap) {
        imageCache.addImageToCache(url.toString(), bitmap);
    }

    private boolean isImageViewRecycled() {
        return (container.get() == null || !container.get().getTag().equals(url.toString()));
    }

}
