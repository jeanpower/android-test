package com.waracle.androidtest;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSONLoader extends AsyncTask<URL, Void, JSONArray> {

    private static final String TAG = JSONLoader.class.getSimpleName();

    private JSONLoaderListener listener;

    public JSONLoader(JSONLoaderListener listener) {
        this.listener = listener;
    }

    @Override
    protected JSONArray doInBackground(URL... params) {

        URL url = params[0];

        HttpURLConnection urlConnection = null;
        String jsonText = "";
        JSONArray array = null;

        try {
            urlConnection = HttpURLConnection.class.cast(url.openConnection());
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            byte[] bytes = StreamUtils.readUnknownFully(in);
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));

            jsonText = new String(bytes, charset);
            array = new JSONArray(jsonText);

        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            StreamUtils.close(urlConnection);
        }

        return array;

    }

    @Override
    public void onPostExecute(JSONArray result) {
        if (result != null && result.length() > 0) {
            listener.onLoadFinished(result);
        } else {
            listener.onError();
        }
    }

    public static String parseCharset(String contentType) {
        if (contentType != null) {
            String[] params = contentType.split(",");

            for (String value : params) {
                String[] pair = value.trim().split("=");

                if (pair.length == 2) {
                    if (pair[0].equals("charset")) {
                        return pair[1];
                    }
                }
            }
        }
        return "UTF-8";
    }

}

