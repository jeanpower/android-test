package com.waracle.androidtest;

import org.json.JSONArray;

public interface JSONLoaderListener {

    void onLoadFinished(JSONArray array);

    void onError();
}
