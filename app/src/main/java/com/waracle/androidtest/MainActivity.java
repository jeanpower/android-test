package com.waracle.androidtest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG = "com.waracle.androidtest.fragmenttag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (networkAvailable()) {
            displayFragment(savedInstanceState);
        } else {
            showError();
        }
    }

    private boolean networkAvailable() {
        ConnectivityManager connectivityManager = ConnectivityManager.class.cast(getSystemService(Context.CONNECTIVITY_SERVICE));
        NetworkInfo network = connectivityManager.getActiveNetworkInfo();
        return network != null && network.isAvailable();
    }

    private void showError() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.network_error_message)
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();

    }

    private void displayFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null && getFragment() == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new CakeListFragment(), FRAGMENT_TAG)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh && getFragment() != null) {
            getFragment().handleRefresh();
        }

        return super.onOptionsItemSelected(item);
    }

    private CakeListFragment getFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        return CakeListFragment.class.cast(fragmentManager.findFragmentByTag(FRAGMENT_TAG));
    }

}
