package com.waracle.androidtest;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class StreamUtils {
    private static final String TAG = StreamUtils.class.getSimpleName();

    private static final int BUFFER = 8092;

    public static byte[] readUnknownFully(InputStream stream) throws IOException {

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] interimBuffer = new byte[BUFFER];
        int bytesRead;

        while ((bytesRead = stream.read(interimBuffer, 0, BUFFER)) != -1) {
            output.write(interimBuffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public static void close(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }
}
